-- ============================================================
-- Author     : Geovane Paz
-- Create date: 01/01/2020
-- Description: exibe hist�rico de altera��es de cidade
-- ============================================================
USE viaflow_geovanegp
GO
CREATE PROCEDURE [dbo].[procedure_teste]
@codigo                 INT,
@data                   DATETIME2 
AS
BEGIN
   
   IF(@codigo = 1)

	   SELECT 
			C.uf as UF,
			COUNT(C.uf) as Qauntidade
		FROM 
			cidades C
		GROUP BY 
			C.uf
	ELSE 
	
		SELECT 
			* 
		FROM 
			log_cidades l
		WHERE 
			Month(l.data) = MONTH(@data)
			AND 
			YEAR(l.data) = YEAR(@data)
			

END
GO