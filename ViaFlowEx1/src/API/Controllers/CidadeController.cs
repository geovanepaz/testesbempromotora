﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Application.Interfaces;
using Application.ViewModels.Cidade;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System;
using Application.ViewModels.BadRequest;

namespace API.Controllers
{
    [ApiController]
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/cidade")]
    [Authorize]
    public class CidadeController : ControllerBase
    {
        private readonly ICidadeAppService appService;

        public CidadeController(ICidadeAppService appService)
        {
            this.appService = appService;
        }

        [HttpPost]
        [ProducesResponseType(typeof(Guid), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        [ProducesResponseType(typeof(BadRequest), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> Adicionar(CidadeRequest request)
        {
            return Ok(await appService.AdicionarAsync(request));
        }

        [HttpPut]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        [ProducesResponseType(typeof(BadRequest), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> Editar(CidadeUpdateRequest request)
        {
            await appService.EditarAsync(request);
            return Ok();
        }

        [HttpDelete]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [Route("{id}")]
        public async Task<IActionResult> Deletar(Guid id)
        {
            await appService.DeletarAsync(id);
            return Ok();
        }

        [HttpGet]
        [ProducesResponseType(typeof(List<CidadeResponse>), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        public async Task<IActionResult> ObterTodosAsync()
        {
            var clientes = await appService.ObterTodosAsync();
            if (clientes == null || clientes.Count() == 0)
            {
                return NotFound();
            }

            return Ok(clientes);
        }
    }
}
