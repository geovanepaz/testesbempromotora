﻿using Application.Interfaces;
using Application.ViewModels.BadRequest;
using Application.ViewModels.Funcionario;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Net;
using System.Threading.Tasks;

namespace API.Controllers
{
    [ApiController]
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/funcionario")]
    [Authorize]
    public class FuncionarioController : ControllerBase
    {
        private readonly IFuncionarioAppService appService;

        public FuncionarioController(IFuncionarioAppService appService)
        {
            this.appService = appService;
        }

        [HttpPost]
        [ProducesResponseType(typeof(Guid), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        [ProducesResponseType(typeof(BadRequest), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> Adicionar(FuncionarioRequest request)
        {
            return Ok(await appService.AdicionarAsync(request));
        }

        [HttpPut]
        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        [ProducesResponseType(typeof(BadRequest), (int)HttpStatusCode.BadRequest)]
        [Route("{id}")]
        public async Task<IActionResult> AlterarCidade(Guid id, FuncionarioUpdateRequest request)
        {
            await appService.AlterarCidadeAsync(id, request.CidadeId);
            return Ok();
        }
    }
}
