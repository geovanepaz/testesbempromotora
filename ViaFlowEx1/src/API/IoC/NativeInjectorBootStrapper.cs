﻿using API.Configuration;
using Application.Interfaces;
using Application.Services;
using Application.ViewModels.Cidade;
using Application.ViewModels.Funcionario;
using Application.ViewModels.Login;
using Application.ViewModels.Validations;
using Domain.Interfaces;
using FluentValidation;
using Infra.Contexts;
using Infra.Repositories;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace API.IoC
{
    public static class NativeInjectorBootStrapper
    {
        public static void ResolveDependencies(this IServiceCollection services)
        {
            //Validator 
            services.AddTransient<IValidator<LoginRequest>, LoginValidator>();
            services.AddTransient<IValidator<UsuarioRequest>, UsuarioValidator>();
            services.AddTransient<IValidator<FuncionarioRequest>, FuncionarioValidator>();
            services.AddTransient<IValidator<CidadeRequest>, CidadeRequestValidator>();
            services.AddTransient<IValidator<CidadeUpdateRequest>, CidadeUpdateRequestValidator>();

            //Repository
            services.AddScoped<ICidadeRepository, CidadeRepository>();
            services.AddScoped<ICidadeLogRepository, CidadeLogRepository>();
            services.AddScoped<IFuncionarioRepository, FuncionarioRepository>();

            //Appsercice
            services.AddScoped<ICidadeAppService, CidadeAppService>();
            services.AddScoped<IFuncionarioAppService, FuncionarioAppService>();

            //Context
            services.AddScoped<ModeloContext>();

            services.AddTransient<IConfigureOptions<SwaggerGenOptions>, ConfigureSwaggerOptions>();

            services.AddTransient<ISqlHelper, SqlHelper>();
        }
    }
}
