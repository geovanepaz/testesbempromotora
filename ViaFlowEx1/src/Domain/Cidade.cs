﻿using Domain.DomainObjects;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class Cidade : Entity
    {
        public string Nome { get; private set; }
        public string Codigo { get; private set; }
        public string UF { get; private set; }

        private readonly List<string> estados;
        private Cidade() { }
        public Cidade(string nome, string codigo, string uf)
        {
            Nome = nome.Trim();
            Codigo = codigo.Trim();
            UF = uf.Trim().ToUpper();
            estados = new List<string>(27);
            PopularEstados();
            Validar();
        }

        public Cidade(Guid id, string nome, string codigo, string uf)
        : this(nome, codigo, uf)
        {
            Id = id;
            Validacoes.ValidarSeNulo(Id, "O campo Id não pode ser nullo");
        }

        /// <summary>
        /// Lança uma exceção caso ja tenha sido feita uma operação no mês.
        /// </summary>
        /// <param name="UltimaOperacao"></param>
        public void ValidarEdicao(DateTime UltimaOperacao)
        {
            if (UltimaOperacao.Month == DateTime.Now.Month)
            {
                throw new DomainException("Não é possivel fazer mais de uma alteração por mês.");
            }
        }

        public void Validar()
        {
            Validacoes.ValidarSeVazio(Nome, "O campo Nome não pode estar vazio");
            Validacoes.ValidarSeVazio(Codigo, "O campo Codigo não pode estar vazio");
            Validacoes.ValidarSeVazio(UF, "O campo Codigo não pode estar vazio");
            Validacoes.ValidarSeContem(estados, UF, "UF inválida");
        }

        private void PopularEstados()
        {
            estados.Add("AC");
            estados.Add("AL");
            estados.Add("AM");
            estados.Add("AP");
            estados.Add("BA");
            estados.Add("CE");
            estados.Add("DF");
            estados.Add("ES");
            estados.Add("GO");
            estados.Add("MA");
            estados.Add("MG");
            estados.Add("MS");
            estados.Add("MT");
            estados.Add("PA");
            estados.Add("PB");
            estados.Add("PE");
            estados.Add("PI");
            estados.Add("PR");
            estados.Add("RJ");
            estados.Add("RN");
            estados.Add("RO");
            estados.Add("RR");
            estados.Add("RS");
            estados.Add("SC");
            estados.Add("SE");
            estados.Add("SP");
            estados.Add("TO");
        }
    }
}
