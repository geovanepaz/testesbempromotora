﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.DomainObjects
{
    public class ProdutoPaginado
    {
        public IEnumerable<Funcionario> Produtos { get; set; }
        public int QuantidadeTotal { get; set; }
        public ProdutoPaginado(IEnumerable<Funcionario> produtos, int quantidadeTotal)
        {
            Produtos = produtos;
            QuantidadeTotal = quantidadeTotal;
        }
    }
}
