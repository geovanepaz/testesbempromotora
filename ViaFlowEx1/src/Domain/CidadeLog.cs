﻿using Domain.DomainObjects;
using System;

namespace Domain
{
    public class CidadeLog : Entity
    {

        public string Nome { get; private set; }
        public string Codigo { get; private set; }
        public string UF { get; private set; }
        public string TipoOperacao { get; private set; }
        public DateTime DataRegistro { get; private set; }

        public Guid CidadeId { get; private set; }

        private CidadeLog() { }
        public CidadeLog(string nome, string codigo, string uf, Guid cidadeId)
        {
            CidadeId = cidadeId;
            Nome = nome.Trim();
            Codigo = codigo.Trim();
            UF = uf.Trim().ToUpper();
            DataRegistro = DateTime.Now;
            TipoOperacao = "insert";
            Validar();
        }

        public void Edicao()
        {
            TipoOperacao = "update";
        }

        public void Exclusao()
        {
            TipoOperacao = "delete";
        }

        public void Validar()
        {
            Validacoes.ValidarSeNulo(CidadeId, "O campo CidadeId não pode ser nullo");
            Validacoes.ValidarSeVazio(Nome, "O campo Nome não pode estar vazio");
            Validacoes.ValidarSeVazio(Codigo, "O campo Codigo não pode estar vazio");
            Validacoes.ValidarSeVazio(UF, "O campo Codigo não pode estar vazio");
        }
    }
}
