﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Domain.Interfaces
{
    public interface ICidadeRepository : IRepository<Cidade>
    {
        /// <summary>
        /// Valida se a cidade ja foi cadastrada.
        /// </summary>
        /// <param name="cidade"></param>
        /// <returns></returns>
        Task<bool> ExisteCidadeAsync(Cidade cidade);

        /// <summary>
        /// Valida se id existe
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<bool> ExisteIdAsync(Guid id);
        Task<IEnumerable<Cidade>> ObterTodosAsync();
        Task<Cidade> ObterPorIdAsync(Guid id);
    }
}
