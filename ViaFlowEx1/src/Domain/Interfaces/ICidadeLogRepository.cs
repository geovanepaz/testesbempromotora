﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Domain.Interfaces
{
    public interface ICidadeLogRepository : IRepository<CidadeLog>
    {
        Task<DateTime> ObterUltimaOperacaoAsync(Guid cidadeId);
    }
}
