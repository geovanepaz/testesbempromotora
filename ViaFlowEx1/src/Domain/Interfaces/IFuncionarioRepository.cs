﻿using Domain.DomainObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Domain.Interfaces
{
    public interface IFuncionarioRepository : IRepository<Funcionario>
    {
        /// <summary>
        /// Verifica se o codigo é unico.
        /// </summary>
        /// <param name="codigo"></param>
        /// <returns></returns>
        Task<bool> ExisteCodigoAsync(string codigo);
        Task<Funcionario> ObterPorIdAsync(Guid id);

        /// <summary>
        /// Verifica se tem funcionarios nesta cidade.
        /// </summary>
        /// <param name="cidadeId"></param>
        /// <returns></returns>
        Task<bool> ContemFuncionariosAsync(Guid cidadeId);
    }
}
