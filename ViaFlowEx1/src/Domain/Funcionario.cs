﻿using Domain.DomainObjects;
using System;
using System.Collections.Generic;

namespace Domain
{
    public class Funcionario : Entity
    {
        public string Nome { get; private set; }
        public string Codigo { get; private set; }
        public string Cargo { get; private set; }
        public Guid CidadeId { get; private set; }
        public Cidade Cidade { get; private set; }

        private readonly List<string> cargos;
        protected Funcionario() { }

        public Funcionario(string nome, string codigo, string cargo, Guid cidadeId)
        {
            Nome = nome.Trim();
            Codigo = codigo;
            Cargo = cargo.Trim().ToUpper();
            CidadeId = cidadeId;
            cargos = new List<string>(3);
            PopularCargos();
            Validar();
        }

        public void AlterarCidade(Guid cidadeId)
        {
            if (Cargo == "VENDEDOR INTERNO")
            {
                throw new DomainException("Vendedor interno não pode alterar a cidade");
            }

            CidadeId = cidadeId;
        }

        private void PopularCargos()
        {
            cargos.Add("VENDEDOR EXTERNO");
            cargos.Add("VENDEDOR INTERNO");
            cargos.Add("SUPERVISOR");
        }

        public void Validar()
        {
            Validacoes.ValidarSeVazio(Nome, "O campo Nome não pode estar vazio");
            Validacoes.ValidarSeVazio(Codigo, "O campo Codigo não pode estar vazio");
            Validacoes.ValidarSeVazio(Cargo, "O campo Codigo não pode estar vazio");
            Validacoes.ValidarSeContem(cargos, Cargo, "Cargo inválido");
        }
    }
}
