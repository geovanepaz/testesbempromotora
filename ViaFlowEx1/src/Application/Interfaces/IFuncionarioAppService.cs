﻿using Application.ViewModels.Funcionario;
using System;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface IFuncionarioAppService : IDisposable
    {
        Task<Guid> AdicionarAsync(FuncionarioRequest request);
        Task AlterarCidadeAsync(Guid funcionarioId, Guid cidadeId);
    }
}
