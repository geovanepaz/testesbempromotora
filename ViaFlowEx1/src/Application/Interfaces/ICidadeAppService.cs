﻿using Application.ViewModels.Cidade;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Application.Interfaces
{
    public interface ICidadeAppService : IDisposable
    {
        Task<Guid> AdicionarAsync(CidadeRequest request);
        Task EditarAsync(CidadeUpdateRequest request);
        Task DeletarAsync(Guid cidadeId);
        Task<IEnumerable<CidadeResponse>> ObterTodosAsync();

    }
}
