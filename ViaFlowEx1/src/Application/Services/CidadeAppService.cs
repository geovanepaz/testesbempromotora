﻿using Application.AutoMapper;
using Application.Interfaces;
using Application.ViewModels.Cidade;
using AutoMapper;
using Domain;
using Domain.DomainObjects;
using Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Application.Services
{
    public class CidadeAppService : ICidadeAppService
    {
        private readonly IMapper _mapper;
        private readonly ICidadeRepository _cidadeRepository;
        private readonly IFuncionarioRepository _funcionarioRepository;
        private readonly ICidadeLogRepository _cidadeLogRepository;

        public CidadeAppService(IMapper mapper, ICidadeRepository cidadeRepository, IFuncionarioRepository funcionarioRepository, ICidadeLogRepository cidadeLogRepository)
        {
            _mapper = mapper;
            _cidadeRepository = cidadeRepository;
            _funcionarioRepository = funcionarioRepository;
            _cidadeLogRepository = cidadeLogRepository;
        }

        public async Task<Guid> AdicionarAsync(CidadeRequest request)
        {
            var cidade = new Cidade(request.Nome, request.Codigo, request.UF);

            ValidarSeJaCadastrada(cidade);

            await _cidadeRepository.InsertAsync(cidade);
            _ = LogInsercaoAsync(cidade);

            return cidade.Id;
        }

        public async Task EditarAsync(CidadeUpdateRequest request)
        {
            var cidade = new Cidade(request.Id, request.Nome, request.Codigo, request.UF);

            ValidarSeExisteCidade(cidade.Id);
            ValidarSeJaCadastrada(cidade);

            DateTime dataUltimaOperacao = ObterUltimaOperacao(cidade.Id);
            cidade.ValidarEdicao(dataUltimaOperacao);

            await _cidadeRepository.UpdateAsync(cidade);
            _ = LogEdicaoAsync(cidade);
        }
        public async Task DeletarAsync(Guid cidadeId)
        {
            ValidarSeContemFuncionarios(cidadeId);

            Cidade cidade = ObterCidade(cidadeId);

            await _cidadeRepository.DeleteAsync(cidade);
            _ = LogExclusaoAsync(cidade);
        }

        public async Task<IEnumerable<CidadeResponse>> ObterTodosAsync()
        {
            var cliente = await _cidadeRepository.ObterTodosAsync();
            return cliente.MapTo<IEnumerable<CidadeResponse>>(_mapper);
        }

        /// <summary>
        /// Traz a data da ultima alteração/inserção efetuada na cidade
        /// </summary>
        /// <param name="cidade"></param>
        /// <returns></returns>
        private DateTime ObterUltimaOperacao(Guid cidadeId)
        {
            return _cidadeLogRepository.ObterUltimaOperacaoAsync(cidadeId).Result;
        }

        private async Task LogInsercaoAsync(Cidade cidade)
        {
            var log = new CidadeLog(cidade.Nome, cidade.Codigo, cidade.UF, cidade.Id);
            await _cidadeLogRepository.InsertAsync(log);
        }

        private async Task LogEdicaoAsync(Cidade cidade)
        {
            var log = new CidadeLog(cidade.Nome, cidade.Codigo, cidade.UF, cidade.Id);
            log.Edicao();
            await _cidadeLogRepository.InsertAsync(log);
        }

        private async Task LogExclusaoAsync(Cidade cidade)
        {
            var log = new CidadeLog(cidade.Nome, cidade.Codigo, cidade.UF, cidade.Id);
            log.Exclusao();
            await _cidadeLogRepository.InsertAsync(log);
        }

        /// <summary>
        /// Verifica se a cidade existe caso contrario lança uma exceção.
        /// </summary>
        /// <param name="cidadeId"></param>
        private void ValidarSeExisteCidade(Guid cidadeId)
        {
            if (!_cidadeRepository.ExisteIdAsync(cidadeId).Result)
            {
                throw new DomainException("Cidade não encontrada");
            }
        }

        /// <summary>
        /// lança uma exceção caso não encontre a cidade.
        /// </summary>
        /// <param name="cidadeId"></param>
        private Cidade ObterCidade(Guid cidadeId)
        {
            Cidade cidade = _cidadeRepository.ObterPorIdAsync(cidadeId).Result;
            if (cidade == null)
            {
                throw new NotFoundException("Cidade não encontrada");
            }

            return cidade;
        }

        /// <summary>
        /// Lança exceção caso cidade ja tenha sido cadastrada.
        /// </summary>
        /// <param name="cidade"></param>
        private void ValidarSeJaCadastrada(Cidade cidade)
        {
            if (_cidadeRepository.ExisteCidadeAsync(cidade).Result)
            {
                throw new DomainException("Cidade ja cadastrada");
            }
        }

        /// <summary>
        /// Lança exceção caso tenha funcionarios na cidade.
        /// </summary>
        /// <param name="cidadeId"></param>
        private void ValidarSeContemFuncionarios(Guid cidadeId)
        {
            if (_funcionarioRepository.ContemFuncionariosAsync(cidadeId).Result)
            {
                throw new DomainException("Existe funcionários cadastrados nesta cidade.");
            }
        }

        public void Dispose()
        {
            _cidadeRepository?.Dispose();
        }
    }
}
