﻿using Application.Interfaces;
using Application.ViewModels.Funcionario;
using AutoMapper;
using Domain;
using Domain.DomainObjects;
using Domain.Interfaces;
using System;
using System.Threading.Tasks;

namespace Application.Services
{
    public class FuncionarioAppService : IFuncionarioAppService
    {
        private readonly IMapper _mapper;
        private readonly IFuncionarioRepository _repository;
        private readonly ICidadeRepository _cidadeRepository;

        public FuncionarioAppService(IMapper mapper, IFuncionarioRepository repository, ICidadeRepository cidadeRepository)
        {
            _mapper = mapper;
            _repository = repository;
            _cidadeRepository = cidadeRepository;
        }

        public async Task<Guid> AdicionarAsync(FuncionarioRequest request)
        {
            var funcionario = new Funcionario(request.Nome, request.Codigo, request.Cargo, request.CidadeId);

            ValidarSeExisteCidade(funcionario.CidadeId);
            ValidarCodigo(funcionario.Codigo);

            var response = await _repository.InsertAsync(funcionario);
            return response.Id;
        }

        public async Task AlterarCidadeAsync(Guid funcionarioId, Guid cidadeId)
        {
            var funcionario = ObterFuncionario(funcionarioId);

            funcionario.AlterarCidade(cidadeId);
            ValidarSeExisteCidade(cidadeId);

            await _repository.UpdateAsync(funcionario);
        }

        /// <summary>
        /// Verifica se a cidade existe caso contrario lança uma exceção.
        /// </summary>
        /// <param name="cidadeId"></param>
        private void ValidarSeExisteCidade(Guid cidadeId)
        {
            if (!_cidadeRepository.ExisteIdAsync(cidadeId).Result)
            {
                throw new DomainException("Cidade não encontrada");
            }
        }

        /// <summary>
        /// lança uma exceção caso o codigo ja tenha sido usado.
        /// </summary>
        /// <param name="cidadeId"></param>
        private void ValidarCodigo(string codigo)
        {
            bool existeCodigo = _repository.ExisteCodigoAsync(codigo).Result;
            if (existeCodigo)
            {
                throw new DomainException("Código ja cadastrado");
            }
        }

        /// <summary>
        /// Lana exceção caso não encontre funcionario.
        /// </summary>
        /// <param name="funcionarioId"></param>
        /// <returns></returns>
        private Funcionario ObterFuncionario(Guid funcionarioId)
        {
            var funcionario = _repository.ObterPorIdAsync(funcionarioId).Result;
            if (funcionario == null)
            {
                throw new DomainException("funcionario não encontrado.");
            }

            return funcionario;
        }

        public void Dispose()
        {
            _repository?.Dispose();
            _cidadeRepository?.Dispose();
        }
    }
}
