﻿using Application.ViewModels.Cidade;
using Application.ViewModels.Funcionario;
using AutoMapper;
using Domain;

namespace Application.AutoMapper
{
    public class DomainToViewModelMappingProfile : Profile
    {
        public DomainToViewModelMappingProfile()
        {
            CreateMap<Cidade, CidadeResponse>();
            CreateMap<Funcionario, FuncionarioResponse>();
        }
    }
}
