﻿using Application.ViewModels.Cidade;
using Application.ViewModels.Funcionario;
using AutoMapper;
using Domain;
using System;

namespace Application.AutoMapper
{
    public class ViewModelToDomainMappingProfile : Profile
    {
        public ViewModelToDomainMappingProfile()
        {
            CreateMap<FuncionarioRequest, Funcionario>()
               .ConstructUsing(p =>
                   new Funcionario(p.Nome, p.Codigo, p.Cargo, p.CidadeId));

            CreateMap<CidadeRequest, Cidade>()
               .ConstructUsing(c => new Cidade(c.Nome, c.Codigo, c.UF));

            CreateMap<Cidade, CidadeLog>()
                .ConstructUsing(c => new CidadeLog(c.Nome, c.Codigo, c.UF, c.Id));
        }
    }
}
