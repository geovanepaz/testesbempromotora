﻿using Application.ViewModels.Cidade;
using FluentValidation;

namespace Application.ViewModels.Validations
{
    public class CidadeRequestValidator : AbstractValidator<CidadeRequest>
    {
        public CidadeRequestValidator()
        {
            RuleFor(o => o.Nome)
                .NotEmpty().WithMessage("{PropertyName} é obrigatório");

            RuleFor(o => o.Codigo)
                .NotEmpty().WithMessage("{PropertyName} é obrigatório");

            RuleFor(o => o.UF)
                .NotEmpty().WithMessage("{PropertyName} é obrigatório");
        }
    }
}
