﻿using Application.ViewModels.Funcionario;
using FluentValidation;

namespace Application.ViewModels.Validations
{
    public class FuncionarioValidator : AbstractValidator<FuncionarioRequest>
    {
        public FuncionarioValidator()
        {
            RuleFor(o => o.Nome)
                .NotEmpty().WithMessage("{PropertyName} é obrigatório");

            RuleFor(o => o.Codigo)
                .NotEmpty().WithMessage("{PropertyName} é obrigatório");

            RuleFor(o => o.Cargo)
                .NotEmpty().WithMessage("{PropertyName} é obrigatória");

            RuleFor(o => o.CidadeId)
                .NotEmpty().WithMessage("{PropertyName} é obrigatório");
        }
    }
}
