﻿using Application.ViewModels.Cidade;
using FluentValidation;

namespace Application.ViewModels.Validations
{
    public class CidadeUpdateRequestValidator : AbstractValidator<CidadeUpdateRequest>
    {
        public CidadeUpdateRequestValidator()
        {
            RuleFor(o => o.Nome)
                .NotEmpty().WithMessage("{PropertyName} é obrigatório");

            RuleFor(o => o.Codigo)
                .NotEmpty().WithMessage("{PropertyName} é obrigatório");

            RuleFor(o => o.UF)
                .NotEmpty().WithMessage("{PropertyName} é obrigatório");
        }
    }
}
