﻿using Application.ViewModels.Cidade;
using System;

namespace Application.ViewModels.Funcionario
{
    public class FuncionarioUpdateRequest
    {
        public Guid CidadeId { get; set; }
    }
}
