﻿using Application.ViewModels.Cidade;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System;
using System.Collections.Generic;

namespace Application.ViewModels.Funcionario
{
    public class FuncionarioResponse
    {
        public Guid Id { get; set; }
        public string Nome { get; set; }
        public string Codigo { get; set; }
        public string Cargo { get; set; }
        public CidadeResponse Cidade { get; set; }
    }
}
