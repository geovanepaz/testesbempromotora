﻿using Application.ViewModels.Cidade;
using System;

namespace Application.ViewModels.Funcionario
{
    public class FuncionarioRequest
    {
        public string Nome { get; set; }
        public string Codigo { get; set; }
        public string Cargo { get; set; }
        public Guid CidadeId { get; set; }
    }
}
