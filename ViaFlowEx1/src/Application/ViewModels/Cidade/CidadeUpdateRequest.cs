﻿using System;

namespace Application.ViewModels.Cidade
{
    public class CidadeUpdateRequest
    {
        public Guid Id { get; set; }
        public string Nome { get; set; }
        public string Codigo { get; set; }
        public string UF { get; set; }
    }
}
