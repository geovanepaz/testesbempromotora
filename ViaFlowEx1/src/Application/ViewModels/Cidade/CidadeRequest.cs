﻿using System;

namespace Application.ViewModels.Cidade
{
    public class CidadeRequest
    {
        public string Nome { get; set; }
        public string Codigo { get; set; }
        public string UF { get; set; }
    }
}
