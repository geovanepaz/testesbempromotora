﻿using Domain;
using Domain.Interfaces;
using Infra.Contexts;
using Microsoft.Extensions.Configuration;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Infra.Repositories
{
    public class FuncionarioRepository : Repository<Funcionario>, IFuncionarioRepository
    {
        private readonly ISqlHelper _sqlHelper;
        public FuncionarioRepository(ModeloContext context, IConfiguration configuration) : base(context)
        {
            _sqlHelper = new SqlHelper(configuration);
        }

        public async Task<bool> ExisteCodigoAsync(string codigo)
        {
            var query = $@"
                           SELECT 
	                            f.nome
                            FROM 
	                            funcionarios f
                            WHERE f.codigo = '{codigo}'
                        ";

            var result = await _sqlHelper.QueryAsync<string>(query);
            return result.Any();
        }

        public async Task<bool> ContemFuncionariosAsync(Guid cidadeId)
        {
            var query = $@"
                           SELECT 
	                            f.nome
                            FROM 
	                            funcionarios f
                            WHERE f.cidade_id = '{cidadeId}'
                        ";

            var result = await _sqlHelper.QueryAsync<string>(query);
            return result.Any();
        }

        public async Task<Funcionario> ObterPorIdAsync(Guid id)
        {
            var query = $@"
                           SELECT 
	                            f.id,
                                f.nome,
                                f.codigo,
                                f.cargo, 
                                f.cidade_id as CidadeId
                            FROM 
	                            funcionarios f
                            WHERE f.id = '{id}'
                        ";

            return await _sqlHelper.QuerySingleAsync<Funcionario>(query);
        }

        public override void Dispose()
        {
            _sqlHelper?.Dispose();
            _context?.Dispose();
        }
    }
}
