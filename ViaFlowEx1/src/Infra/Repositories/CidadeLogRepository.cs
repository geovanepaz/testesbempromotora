﻿using Domain;
using Domain.Interfaces;
using Infra.Contexts;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualBasic;
using System;
using System.Threading.Tasks;

namespace Infra.Repositories
{
    public class CidadeLogRepository : Repository<CidadeLog>, ICidadeLogRepository
    {
        private readonly ISqlHelper _sqlHelper;

        public CidadeLogRepository(ModeloContext context, IConfiguration configuration) : base(context)
        {
            _sqlHelper = new SqlHelper(configuration);
        }

        public async Task<DateTime> ObterUltimaOperacaoAsync(Guid cidadeId)
        {
            var query = $@"
                             SELECT 
		                        l.data
	                        FROM 
		                        log_cidades l
	                        WHERE 
		                        l.cidade_id = '{cidadeId}'
	                        ORDER BY l.data DESC
                        ";

            return await _sqlHelper.QuerySingleAsync<DateTime>(query);
        }
    }
}
