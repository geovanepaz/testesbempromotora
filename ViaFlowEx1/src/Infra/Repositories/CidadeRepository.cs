﻿using Domain;
using Domain.Interfaces;
using Infra.Contexts;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Infra.Repositories
{
    public class CidadeRepository : Repository<Cidade>, ICidadeRepository
    {
        private readonly ISqlHelper _sqlHelper;

        public CidadeRepository(ModeloContext context, IConfiguration configuration) : base(context)
        {
            _sqlHelper = new SqlHelper(configuration);
        }

        public async Task<bool> ExisteCidadeAsync(Cidade cidade)
        {
            var query = $@"
                            SELECT 
		                        c.nome
	                        FROM 
		                        cidades c
	                        WHERE c.nome = '{cidade.Nome}'
                            AND c.uf = '{cidade.UF}'
                            AND c.codigo = '{cidade.Codigo}'
                        ";

            var result = await _sqlHelper.QueryAsync<string>(query);
            return result.Any();
        }

        public async Task<bool> ExisteIdAsync(Guid id)
        {
            var query = $@"
                            SELECT 
		                        c.nome
	                        FROM 
		                        cidades c
	                        WHERE c.id = '{id}'
                        ";

            var result = await _sqlHelper.QueryAsync<string>(query);
            return result.Any();
        }

        public async Task<IEnumerable<Cidade>> ObterTodosAsync()
        {
            var query = $@"
                            SELECT 
		                        *
	                        FROM 
		                        cidades 
                        ";

            return await _sqlHelper.QueryAsync<Cidade>(query);
        }

        public async Task<Cidade> ObterPorIdAsync(Guid id)
        {
            var query = $@"
                            SELECT 
                                c.id,
		                        c.nome, 
                                c.codigo,
                                c.uf
	                        FROM 
		                        cidades c
                             WHERE c.id = '{id}';
                        ";
            return await _sqlHelper.QuerySingleAsync<Cidade>(query);
        }

        public override void Dispose()
        {
            _sqlHelper?.Dispose();
            _context?.Dispose();
        }
    }
}
