﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Domain;

namespace Infra.Mappings
{
    public class FuncionarioMapping : IEntityTypeConfiguration<Funcionario>
    {
        public void Configure(EntityTypeBuilder<Funcionario> builder)
        {
            builder.HasKey(c => c.Id);

            builder.Property(c => c.CidadeId)
                .IsRequired()
                .HasColumnName("cidade_id")
                .HasColumnType("uniqueidentifier");

            builder.Property(c => c.Id)
                .HasColumnName("id");

            builder.Property(c => c.Nome)
                .IsRequired()
                .HasColumnName("nome")
                .HasColumnType("varchar(255)");

            builder.Property(c => c.Codigo)
                .IsRequired()
                .HasColumnName("codigo")
                .HasColumnType("varchar(500)");

            builder.ToTable("funcionarios");
        }
    }
}
