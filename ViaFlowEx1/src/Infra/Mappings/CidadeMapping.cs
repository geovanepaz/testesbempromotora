﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Domain;

namespace Infra.Mappings
{
    public class CidadeMapping : IEntityTypeConfiguration<Cidade>
    {
        public void Configure(EntityTypeBuilder<Cidade> builder)
        {
            builder.HasKey(c => c.Id);

            builder.Property(c => c.Id)
                .HasColumnName("id");

            builder.Property(c => c.Nome)
                .IsRequired()
                .HasColumnName("nome")
                .HasColumnType("varchar(255)");

            builder.Property(c => c.Codigo)
                .IsRequired()
                .HasColumnName("codigo")
                .HasColumnType("varchar(255)");

            builder.Property(c => c.UF)
              .IsRequired()
              .HasColumnName("uf")
              .HasColumnType("char(2)");

            builder.ToTable("cidades");
        }
    }
}
