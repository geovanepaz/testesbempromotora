﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Domain;

namespace Infra.Mappings
{
    public class CidadeLogMapping : IEntityTypeConfiguration<CidadeLog>
    {
        public void Configure(EntityTypeBuilder<CidadeLog> builder)
        {
            builder.HasKey(c => c.Id);

            builder.Property(c => c.Id)
                .HasColumnName("id");

            builder.Property(c => c.CidadeId)
                .IsRequired()
                .HasColumnName("cidade_id")
                .HasColumnType("uniqueidentifier");

            builder.Property(c => c.Nome)
                .IsRequired()
                .HasColumnName("nome")
                .HasColumnType("varchar(255)");

            builder.Property(c => c.Codigo)
                .IsRequired()
                .HasColumnName("codigo")
                .HasColumnType("varchar(255)");

            builder.Property(c => c.UF)
                .IsRequired()
                .HasColumnName("uf")
                .HasColumnType("char(2)");

            builder.Property(c => c.TipoOperacao)
                .IsRequired()
                .HasColumnName("operacao")
                .HasColumnType("varchar(10)");

            builder.Property(c => c.DataRegistro)
                .IsRequired()
                .HasColumnName("data")
                .HasColumnType("datetime2");

            builder.ToTable("log_cidades");
        }
    }
}
