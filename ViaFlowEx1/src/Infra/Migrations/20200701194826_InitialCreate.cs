﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Infra.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "cidades",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    nome = table.Column<string>(type: "varchar(255)", nullable: false),
                    codigo = table.Column<string>(type: "varchar(255)", nullable: false),
                    uf = table.Column<string>(type: "char(2)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_cidades", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "log_cidades",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    nome = table.Column<string>(type: "varchar(255)", nullable: false),
                    codigo = table.Column<string>(type: "varchar(255)", nullable: false),
                    uf = table.Column<string>(type: "char(2)", nullable: false),
                    operacao = table.Column<string>(type: "varchar(10)", nullable: false),
                    data = table.Column<DateTime>(type: "datetime2", nullable: false),
                    cidade_id = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_log_cidades", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "funcionarios",
                columns: table => new
                {
                    id = table.Column<Guid>(nullable: false),
                    nome = table.Column<string>(type: "varchar(255)", nullable: false),
                    codigo = table.Column<string>(type: "varchar(500)", nullable: false),
                    Cargo = table.Column<string>(nullable: true),
                    cidade_id = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_funcionarios", x => x.id);
                    table.ForeignKey(
                        name: "FK_funcionarios_cidades_cidade_id",
                        column: x => x.cidade_id,
                        principalTable: "cidades",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_funcionarios_cidade_id",
                table: "funcionarios",
                column: "cidade_id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "funcionarios");

            migrationBuilder.DropTable(
                name: "log_cidades");

            migrationBuilder.DropTable(
                name: "cidades");
        }
    }
}
